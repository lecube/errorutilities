//
//  ErrorProvider.swift
//  
//
//  Created by Sebastien Bastide on 27/12/2021.
//

import Foundation

public typealias ErrorCode = Int

public protocol ErrorProvider {
    static func error(forCode code: ErrorCode) -> NSError
}
