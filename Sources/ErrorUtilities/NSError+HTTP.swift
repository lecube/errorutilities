//
//  NSError+HTTP.swift
//  
//
//  Created by Sebastien Bastide on 27/12/2021.
//

import Foundation

extension NSError {

    /**
     - returns: true if the error chain contains an error with a code between 400 and 499, false otherwise
     */
    public func isClientError() -> Bool {
        isUnderlyingError(within: NSMakeRange(400, 100))
    }

    /**
        - returns: true if the error chain contains an error with a code between 500 and 599, false otherwise
     */
    public func isServerError() -> Bool {
        isUnderlyingError(within: NSMakeRange(500, 100))
    }

    /**
        - returns: true if the error chain contains an error with a code equal matching one of the following codes:
            - 408 : request timeout
            - 504 : gateway timeout
            - 598 : network read timeout
            - 599 : network connect timeout
     */
    public func isTimeoutError() -> Bool {
        isPresentUnderlyingError(code: NSURLErrorTimedOut) ||
            isPresentUnderlyingError(code: 408) || // request timeout
            isPresentUnderlyingError(code: 504) || // gateway timeout
            isPresentUnderlyingError(code: 598) || // network read timeout
            isPresentUnderlyingError(code: 599)    // network connect timeout
    }

    /**
     - returns: true if the error chain contains an error with a code equal to NSURLErrorNotConnectedToInternet, false otherwise
     */
    public func isConnectivityError() -> Bool {
        isPresentUnderlyingError(code: NSURLErrorNotConnectedToInternet)
    }

    /**
     - returns: true if the error chain contains an error equal to 429, false otherwise
     */
    public func isTooManyRequestsError() -> Bool {
        isPresentUnderlyingError(code: 429)
    }

    /**
     - returns: true if the error chain contains an error equal to 401, false otherwise
     */
    public func isAuthorizationError() -> Bool {
        isPresentUnderlyingError(code: 401)
    }
}
