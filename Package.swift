// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ErrorUtilities",
    platforms: [.iOS(.v13)], 
    products: [
        .library(name: "ErrorUtilities", targets: ["ErrorUtilities"]),
    ],
    dependencies: [],
    targets: [
        .target(name: "ErrorUtilities", dependencies: []),
        .testTarget(name: "ErrorUtilitiesTests", dependencies: ["ErrorUtilities"]),
    ]
)
